import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

/**
 * Author: plouzeau
 * Date: 2013-07-16
 * Time: 18:26
 */
public class OrderImplTest {

	private Order o1;

	@Test
	public void testOneMore() throws Exception {
		o1.oneMore(Beverage.COFFEE);
		Assert.assertEquals(o1.getCount(Beverage.COFFEE), 1);
	}

	@Test
	public void testCancelOne() throws Exception {
		o1.oneMore(Beverage.COFFEE);
		o1.cancelOne(Beverage.COFFEE);
		Assert.assertEquals(o1.getCount(Beverage.COFFEE), 0);
	}

	@Test
	public void testOneMoreThrice() throws Exception {
		o1.oneMore(Beverage.COFFEE);
		o1.oneMore(Beverage.JUICE);
		o1.oneMore(Beverage.COFFEE);

		Assert.assertEquals(o1.getCount(Beverage.COFFEE), 2);
		Assert.assertEquals(o1.getCount(Beverage.JUICE), 1);

		for(Iterator<Beverage> i = o1.getIteratorOnBeverages(); i.hasNext();) {

		}


	}

	@Before
	public void setup() {
		o1 = new OrderImpl();
	}
}
