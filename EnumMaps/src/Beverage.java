/**
 * Author: plouzeau
 * Date: 2013-07-16
 * Time: 15:24
 */
public enum Beverage {
	COFFEE,
	TEA,
	LEMONADE,
	JUICE
}
