/**
 * Author: plouzeau
 * Date: 2013-07-16
 * Time: 15:19
 */
public enum Extra {
	CREAM,
	MILK,
	CARAMEL,
	CINNAMON
}

