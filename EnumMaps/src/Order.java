import java.util.Iterator;

/**
 * Author: plouzeau
 * Date: 2013-07-16
 * Time: 15:38
 */
public interface Order {

	/**
	 * @param b The beverage kind to count
	 * @return The number of a given beverage b in the order
	 */
	public int getCount(Beverage b);

	/**
	 * Add another beverage of kind b to the order
	 *
	 * @param b The beverage kind to add
	 */
	public void oneMore(Beverage b);

	/**
	 * Remove one beverage b from the order
	 *
	 * @param b The beverage to remove
	 */
	public void cancelOne(Beverage b);

	/**
	 *
	 * @return  an iterator on the beverage kinds in the order
	 */
	public Iterator<Beverage> getIteratorOnBeverages();


}
