import java.util.EnumMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Author: plouzeau
 * Date: 2013-07-16
 * Time: 15:22
 */
public class OrderImpl implements Order {

	private Map<Beverage, Integer> items = new EnumMap<Beverage, Integer>(Beverage.class);

	/**
	 *
	 * @param b The beverage kind to count
	 * @return  Number of beverage b in order
	 */
	@Override
	public int getCount(Beverage b) {
		if (b == null) throw new NullPointerException("b");

		return items.get(b);  //To change body of implemented methods use File | Settings | File Templates.
	}

	/**
	 * Add another beverage of kind b to the order
	 *
	 * @param b     The beverage kind to increment
	 * @throws NullPointerException if b is null
	 */
	@Override
	public void oneMore(Beverage b) {
		if (b == null) throw new NullPointerException("b");

		Integer nbOrdered = items.get(b);
		if (nbOrdered == null) {
			items.put(b, 1);
		} else {
			items.put(b, nbOrdered + 1);
		}

	}

	/**
	 * Remove one beverage b from the order
	 *
	 * @param b        The beverage kind to decrement
	 * @throws NullPointerException     is b is null
	 * @throws IllegalArgumentException is there is no b to cancel
	 */
	@Override
	public void cancelOne(Beverage b) {
		if (b == null) throw new NullPointerException("b");
		if (items.get(b) == null) throw new IllegalArgumentException("b");
		if (items.get(b) == 0) throw new IllegalArgumentException("b");

		Integer nbOrdered = items.get(b);
		items.put(b, nbOrdered - 1);
	}

	/**
	 *
	 * @return   an iterator on the veregaes in the order
	 */
	@Override
	public Iterator<Beverage> getIteratorOnBeverages() {
		return items.keySet().iterator();
	}
}
