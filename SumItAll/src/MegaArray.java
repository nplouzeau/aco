/**
 * Author: plouzeau
 * Date: 2013-05-26
 * Time: 18:32
 */
public class MegaArray {

	final int ARRAY_SIZE = 1000000;

	int myArray[] = new int[ARRAY_SIZE];

	void initialize() {
		for (int i = 0; i < ARRAY_SIZE; i++) {
			myArray[i] = 2 * i;
		}
	}

	long sum() {
		long accum = 0L;
		for (int i = 0; i < ARRAY_SIZE; i++) {
			accum += myArray[i];
		}
		return accum;
	}
}
