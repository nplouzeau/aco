package test;

import fr.istic.nplouzeau.dimensions.Length;
import fr.istic.nplouzeau.dimensions.LengthFactory;
import fr.istic.nplouzeau.dimensions.LengthUnit;
import junit.framework.Assert;
import org.junit.Test;

/**
 * Author: PLOUZEAU, Noël
 * Date: 2013-07-30
 * Time: 17:05
 */
public class LengthFactoryTest {
	@Test
	public void testMake() throws Exception {
		Length l = LengthFactory.make(1.0E01, LengthUnit.FOOT);
		Assert.assertEquals(l.getNumericalValue(LengthUnit.FOOT), 1.0E01);
		Assert.assertTrue(Math.abs(l.getNumericalValue(LengthUnit.NAUTICAL_MILE) - 1.64578834E-03) < 1E-8);

	}

	@Test
	public void testAdd() throws Exception {
		Length l1 = LengthFactory.make(1.0E01, LengthUnit.FOOT);
		Length l2 = LengthFactory.make(1.0E01, LengthUnit.FOOT);
		Assert.assertTrue(Math.abs(l1.add(l2).getNumericalValue(LengthUnit.FOOT)
				                           - 2E+01) < 1E-08);
	}
}
