package fr.istic.nplouzeau.dimensions;

/**
 * Author: PLOUZEAU, Noël
 * Date: 2013-07-30
 * Time: 15:02
 */
public enum LengthUnit {
	METER,
	INCH,
	FOOT,
	YARD,
	IMPERIAL_MILE,
	NAUTICAL_MILE;

}
