package fr.istic.nplouzeau.dimensions;

/**
 * Author: PLOUZEAU, Noël
 * Date: 2013-07-30
 * Time: 15:01
 */
public class LengthFactory {


	static public Length make(double value, LengthUnit u) {
		return new LengthImpl(value, u);
	}


	private static class LengthImpl implements Length {

		// Use the international meter unit as the internal unit for measurement

		private final double valueInMeters;


		LengthImpl(double value, LengthUnit u) {
			switch (u) {
				case METER:
					valueInMeters = value;
					break;
				case INCH:
					valueInMeters = value * 2.54E-02;
					break;
				case FOOT:
					valueInMeters = value * 3.048E-01;
					break;
				case YARD:
					valueInMeters = value * 9.144E-01;
					break;
				case IMPERIAL_MILE:
					valueInMeters = value * 1.609344E03;
					break;
				case NAUTICAL_MILE:
					valueInMeters = value * 1.852E03;
					break;
				default:
					throw new IllegalArgumentException("length unit");
			}

		}


		/**
		 * Read accessor
		 * @param u    Unit to use for value returned
		 * @return     The length as a double in the u unit
		 */
		@Override
		public double getNumericalValue(LengthUnit u) {
			switch (u) {
				case METER:
					return valueInMeters;
				case INCH:
					return valueInMeters / 2.54E-02;
				case FOOT:
					return valueInMeters / 3.048E-01;
				case YARD:
					return valueInMeters / 9.144E-01;
				case IMPERIAL_MILE:
					return valueInMeters / 1.609344E03;
				case NAUTICAL_MILE:
					return valueInMeters / 1.852E03;
				default:
					throw new IllegalArgumentException("length unit");
			}

		}

		/**
		 * Creates a length that is the sum of this and l
		 *
		 * @param l
		 * @return a new length sum of this and l
		 */
		@Override
		public Length add(Length l) {
			return new LengthImpl(this.valueInMeters + l.getNumericalValue(LengthUnit.METER),
					                     LengthUnit.METER);
		}


	}

}
