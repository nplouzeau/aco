package fr.istic.nplouzeau.dimensions;

/**
 * Author: PLOUZEAU, Noël
 * Date: 2013-07-30
 * Time: 14:56
 */
public interface Length {

	public double getNumericalValue(LengthUnit u);

	/**
	 * Creates a length that is the sum of this and l
	 * @param l
	 * @return  a new length sum of this and l
	 */
	public Length add(Length l);

}
